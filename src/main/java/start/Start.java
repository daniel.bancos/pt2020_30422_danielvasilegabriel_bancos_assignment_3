package start;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import bll.SupplierBLL;
import dao.ClientDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
	
	/**
	 * The executer contains the methods to parse the text from a command file and execute methods
	 */
	private Executer executer = new Executer();
	
	public static void main(String[] args) throws SQLException {
		Start start = new Start();
		File file = new File(args[0]); 
	  BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			System.out.println("Error: file does not exist!");
			return;
		} 
		
		String st;
		try {
			while((st = br.readLine()) != null) {
				//System.out.println(st);
				start.executer.parseCommand(st);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
