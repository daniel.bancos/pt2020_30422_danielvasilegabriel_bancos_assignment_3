package start;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfWriter;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import bll.SupplierBLL;
import model.Client;
import model.Order;
import model.Product;

public class Executer {
	private String command = "";
	
	/**
	 * fileCounter represents the number of the generated file when an order is inserted (i.e. number of the bill)
	 */
	private int fileCounter = 0;
	
	
	/**
	 * clientReportNb represents the number of the client report
	 */
	private int clientReportNb = 0;
	
	/**
	 * productReportNb represents the number of the product report
	 */
	private int productReportNb = 0;
	
	/**
	 * orderReportNb represents the number of the order report
	 */
	private int orderReportNb = 0;
	
	/**
	 * Identifies which is the command to be executed
	 * @param s
	 */
	
	public void parseCommand(String s) {
		int index = getCmd(s);
		switch(command) {
			case "Insert client":
				System.out.println(s);
				execInsertClient(s, index);
			
			break;
			case "Insert product":
				System.out.println(s);
				execInsertProduct(s, index);
				
			break;
			case "Delete client":
				System.out.println(s);
				execDeleteClient(s, index);
			
			break;
			case "Delete product":
				System.out.println(s);
				execDeleteProduct(s, index);
				
			break;
			case "Order":
				System.out.println(s);
				execOrder(s, index, fileCounter);
				fileCounter++;
			
			break;
			case "Report client":
				System.out.println(s);
				execReportClient(clientReportNb);
				clientReportNb++;
			
			break;
			case "Report order":
				System.out.println(s);
				execReportOrder(orderReportNb);
				orderReportNb++;
			break;
			case "Report product":
				System.out.println(s);
				execReportProduct(productReportNb);
				productReportNb++;
				
			break;
			default:
					// do nothing
		}
		command = "";
	}
	
	/**
	 * Forms a string based on an input string s
	 * @param s
	 * @return command
	 */
	
	public int getCmd(String s) {
		int i = 0;
		while(i < s.length() && s.charAt(i) != ':') {
			command += s.charAt(i);
			i++;
		}
		
		return i;
	}
	
	/**
	 * Extracts the parameters form the command string and executes the appropriate clientBll method for insertion
	 * @param s
	 * @param index
	 */
	
	void execInsertClient(String s, int index){
		index += 2;
		String name = "";
		String address = "";
		while(s.charAt(index) != ',') {
			name += s.charAt(index);
			index++;
		}
		index += 2;
		while(index < s.length()) {
			address += s.charAt(index);
			index++;
		}
		ClientBLL clientBll = new ClientBLL();
		clientBll.insertClient(new Client(name, address));
	}
	
	/**
	 * Extracts the parameters from the command string and executes the appropriate productBll method for insertion
	 * @param s
	 * @param index
	 */
	
	void execInsertProduct(String s, int index){
		String name = "";
		String quantity = "";
		String price = "";
		String supplier = "";
		
		index += 2;
		while(s.charAt(index) != ',') {
			name += s.charAt(index);
			index++;
		}
		index += 2;
		
		while(s.charAt(index) != ',') {
			quantity += s.charAt(index);
			index++;
		}
		index += 2;
		
		if(Integer.parseInt(quantity) < 0) {
			System.out.println("Invalid input");
			return;
		}
		
		while(s.charAt(index) != ',') {
			price += s.charAt(index);
			index++;
		}
		index += 2;
		
		if(Integer.parseInt(price) < 0) {
			System.out.println("Invalid input");
			return;
		}
		
		while(index < s.length()) {
			supplier += s.charAt(index);
			index++;
		}
		System.out.println(supplier);
		SupplierBLL supplierBll = new SupplierBLL();
		ProductBLL productBll = new ProductBLL();
		int id = supplierBll.getSupplierId(supplier);
		System.out.println(id);
		if(id < 0) {
			return;
		}

		productBll.insertProduct(new Product(name, Integer.parseInt(quantity), Integer.parseInt(price), id));
	}
	
	/**
	 * Extracts the parameters from the command string and executes the appropriate clientBll method for deletion
	 * @param s
	 * @param index
	 */
	
	public void execDeleteClient(String s, int index){
		String name = "";
		index += 2;
		
		while(index < s.length()) {
			name += s.charAt(index);
			index++;
		}
		
		System.out.println(name);
		ClientBLL clientBll = new ClientBLL();
		clientBll.delete(name);
	}
	
	/**
	 * Extracts the parameters from the command string and executes the appropriate productBll method for deletion
	 * @param s
	 * @param index
	 */
	
	public void execDeleteProduct(String s, int index){
		String name = "";
		index += 2;
		
		while(index < s.length()) {
			name += s.charAt(index);
			index++;
		}
		
		System.out.println(name);
		ProductBLL productBll = new ProductBLL();
		productBll.delete(name);
	}
	
	/**
	 * Extracts the parameters from the command string and executes the appropriate orderBll method for insertion if the 
	 * product with id porductId and the client with the id clientId exist
	 * @param s
	 * @param index
	 */
	
	public void execOrder(String s, int index, int fileNb) {
		String client = "";
		String product = "";
		String quantity = "";
		
		index += 2;
		while(s.charAt(index) != ',') {
			client += s.charAt(index);
			index++;
		}
		index += 2;
		
		while(s.charAt(index) != ',') {
			product += s.charAt(index);
			index++;
		}
		index += 2;
		
		while(index < s.length()) {
			quantity += s.charAt(index);
			index++;
		}
		
		if(Integer.parseInt(quantity) < 0) {
			System.out.println("Invalid input");
			return;
		}
		
		ClientBLL clientBll = new ClientBLL();
		ProductBLL productBll = new ProductBLL();
		OrderBLL orderBll = new OrderBLL();
		int clientId = clientBll.getId(client);
		int productId = productBll.getId(product);
		if(clientId < 0 || productId < 0) {
			return;
		}
		orderBll.insertOrder(new Order(clientId, productId, Integer.parseInt(quantity)), fileNb);
		
	}
	
	/**
	 * Calls the clientBLL's reprotClient() method
	 * @param fileNb
	 */
	
	public void execReportClient(int fileNb) {
		
		ClientBLL clientBll = new ClientBLL();
		clientBll.reportClient(fileNb);
	}
	
	/**
	 * Calls the productBLL's reprotProduct() method
	 * @param fileNb
	 */
	
	public void execReportProduct(int fileNb) {
			
		ProductBLL productBll = new ProductBLL();
		productBll.reportProduct(fileNb);
	}
	
	/**
	 * Calls the orderBLL's reprotOrder() method
	 * @param fileNb
	 */
	
	public void execReportOrder(int fileNb) {
		
		OrderBLL orderBll = new OrderBLL();
		orderBll.reportOrder(fileNb);
	}
}
