package bll;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import dao.ClientDAO;
import model.Client;

public class ClientBLL {
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
//	private List<Validator<Student>> validators;
//
//	public StudentBLL() {
//		validators = new ArrayList<Validator<Student>>();
//		validators.add(new EmailValidator());
//		validators.add(new StudentAgeValidator());
//	}
	
	/**
	 * Calls the method reprotClient of the ClientDAO class
	 * @param fileNb representing the number of the file generated
	 */
	
	public void reportClient(int fileNb) {
		ClientDAO dao = new ClientDAO();
		dao.reportClient(fileNb);
	}
	
	/**
	 * Calls the method findById() of the ClientDAO class
	 * @param id representing the id of the searched client
	 * @return object of type client
	 */
	
	public Client findClientById(int id) {
		ClientDAO dao = new ClientDAO();
		Client cl = dao.findById(id);
		if (cl == null) {
			throw new NoSuchElementException("The student with id =" + id + " was not found!");
		}
		return cl;
	}
	
	/**
	 * Calls the method checkIfExists() of the ClientDAO class
	 * @param name representing the name of the searched client
	 * @return an integer that represents the id of the searched object
	 */
	
	public int getId(String name) {
		ClientDAO dao = new ClientDAO();
		int id = dao.checkIfExists(name);
		return id;
	}
	
	/**
	 * Calls the method insert() of the ClientDAO class if the client client exists
	 * @param client representing the client to be inserted
	 */
	
	public void insertClient(Client client) {
//		for (Validator<Student> v : validators) {
//			v.validate(student);
//		}
		ClientDAO dao = new ClientDAO();
		if(dao.checkIfExists(client.getName()) == -1) {
			dao.insert(client);
		} else {
			System.out.println("Client " + client.getName() + " already exists");
		}
	}
	
	/**
	 * Calls the method delete() of the ClientDAO class. Also deletes any order that has as clientId the id of the client having the name name
	 * @param name representing the name of the client to be deleted
	 */
	
	public void delete(String name) {
		ClientDAO dao = new ClientDAO();
		int id = dao.checkIfExists(name);
		System.out.println(id);
		
		if(id != -1) {
			dao.deleteFromOrder(id);
		}
		
		if(id != -1) {
			dao.delete(id);
		} else {
			System.out.println("The student " + name + " was not found!");
		}
	}
}
