package bll;

import dao.SupplierDAO;

public class SupplierBLL {
	
	/**
	 * Calls the method getId() of the Supplier class
	 * @param name representing the name of the searched supplier
	 * @return the id of the supplier that has the name name
	 */
	
	public int getSupplierId(String name) {
		SupplierDAO dao = new SupplierDAO();
		int id = dao.getId(name);
		if(id == -1) {
			System.out.println("Invalid supplier name");
		}
		return id;
	}
}
