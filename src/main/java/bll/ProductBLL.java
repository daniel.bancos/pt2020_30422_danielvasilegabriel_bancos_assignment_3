package bll;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import dao.ClientDAO;
import dao.ProductDAO;
import model.Product;

public class ProductBLL {
	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());

	/**
	 * Calls the method reprotProduct() of the ProductDAO class
	 * @param fileNb representing the number of the file generated
	 */
	
	public void reportProduct(int fileNb) {
		ProductDAO dao = new ProductDAO();
		dao.reportProduct(fileNb);
	}
	
	/**
	 * Calls the method findByIdRem() of the ProductDAO class
	 * @param id representing the id of the searched product
	 * @return
	 */
	
	public Product findProductById(int id) {
		ProductDAO dao = new ProductDAO();
		Product cl = dao.findByIdRem(id);
		if (cl == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return cl;
	}
	
	/**
	 * Calls the method checkIfExists() of the ProductDAO class
	 * @param name representing the name of the searched product
	 * @return id of the product that has the name name if it exists, or -1 otherwise
	 */
	
	public int getId(String name) {
		int id = -1;
		ProductDAO dao = new ProductDAO();
		Product p = dao.checkIfExists(name);
		if(p != null) {
			id = p.getId();
		}
		
		return id;
	}
	
	/**
	 * Calls the method insert() of the ProductDAO class if a product with the same name does not exist, or the method update() otherwise
	 * @param product representing the product to be inserted
	 */
	
	public void insertProduct(Product product) {
		ProductDAO dao = new ProductDAO();
		Product p = dao.checkIfExists(product.getName());
		if(p == null) {
			dao.insert(product);
		} else {
			if(product.getPrice() != p.getPrice() || product.getSupplier() != p.getSupplier()) {
			}
			dao.update(new Product(p.getId(), product.getName(), p.getQuantity() + product.getQuantity(), product.getPrice(), product.getSupplier()));
		}
	}
	
	/**
	 * Calls the method deleteFromOrder of the ProductDAO() class if the product with the name name exists
	 * @param name representing the name of the product to be deleted
	 */
	
	public void delete(String name) {
		ProductDAO dao = new ProductDAO();
		Product product = dao.checkIfExists(name);
		
		if(product != null) {
			dao.deleteFromOrder(product.getId());
		}
		
		if(product != null) {
			dao.delete(product.getId());
		} else {
			System.out.println("The product " + name + " was not found!");
		}
	}
}
