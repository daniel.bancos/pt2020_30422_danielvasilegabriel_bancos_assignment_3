package bll;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;

public class OrderBLL {
	
	/**
	 * Calls the method reportOrder of the OrderDAO class
	 * @param fileNb representing the number of the file generated
	 */
	
	public void reportOrder(int fileNb) {
		OrderDAO dao = new OrderDAO();
		dao.reportOrder(fileNb);
	}
	
	/**
	 * Creates a PDF file containing the information about an order, a.k.a the bill
	 * @param client
	 * @param product
	 * @param quantity
	 * @param price
	 * @param fileNb
	 */
	
	private void createBill(String client, String product, int quantity, int price, int fileNb) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Order_Bill_" + fileNb + ".pdf"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		 
		document.open();
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk0 = new Chunk("ORDER", font);
		Chunk chunk1 = new Chunk("CLIENT: " + client, font);
		Chunk chunk2 = new Chunk("PRODUCT: " + product, font);
		Chunk chunk3 = new Chunk("QUANTITY: " + Integer.toString(quantity), font);
		Chunk chunk4 = new Chunk("TOTAL: " + Integer.toString(quantity * price), font);
		 
		try {
			document.add(new Paragraph(chunk0));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph(chunk1));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph(chunk2));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph(chunk3));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph(chunk4));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	/**
	 * Creates a PDF file with an under-stock message
	 * @param fileNb representing the number of the file generated
	 */
	
	private void createUnderStock(int fileNb) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("UnderStock_" + fileNb + ".pdf"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		 
		document.open();
		 
		try {
			document.add(new Paragraph("ORDER FAILED: there are not enough products on stock"));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	/**
	 * Calls the method isnertOrder of the OrderDAO class, if the read productId and clientId exist and also create a bill. If they
	 * do not exist, a under-stock file is created
	 * @param order
	 * @param fileNb
	 */
	
	public void insertOrder(Order order, int fileNb) {
		
		OrderDAO dao = new OrderDAO();
		ProductDAO productDAO = new ProductDAO();
		ClientDAO clientDAO = new ClientDAO();
		Product product = productDAO.findByIdRem(order.getproductId());
		Client client = clientDAO.findByIdRem(order.getclientId());
		if(product.getQuantity() < order.getQuantity()) {
			createUnderStock(fileNb);
		}
		System.out.println(product.getName());
		System.out.println(product.getQuantity());
		if(product != null && client != null) {
			productDAO.update(new Product(product.getId(), product.getName(), product.getQuantity() - order.getQuantity(), product.getPrice(), product.getSupplier()));
		}
		
		createBill(client.getName(), product.getName(), order.getQuantity(), product.getPrice(), fileNb);
		dao.insert(order);
	}
}
