package model;

/**
 * The class representing the supplier table
 * @author A
 *
 */

public class Supplier {
	private int id;
	private String name;
	
	public Supplier(String name) {
		this.name = name;
	}
	
	public Supplier(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Supplier() {
		//empty
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + "]";
	}
}
