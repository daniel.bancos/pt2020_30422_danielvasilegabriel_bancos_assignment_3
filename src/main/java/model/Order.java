package model;

/**
 * The class representing the order table
 * @author A
 *
 */

public class Order {
	private int id;
	private int clientId;
	private int productId;
	private int quantity;
	
	public Order(int id, int clientId, int productId, int quantity) {
		super();
		this.id = id;
		this.clientId = clientId;
		this.productId = productId;
		this.quantity = quantity;
	}

	public Order(int clientId, int productId, int quantity) {
		super();
		this.clientId = clientId;
		this.productId = productId;
		this.quantity = quantity;
	}
	
	public Order() {
		// empty
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getclientId() {
		return clientId;
	}

	public void setclientId(int clientId) {
		this.clientId = clientId;
	}
	
	public int getproductId() {
		return productId;
	}

	public void setproductId(int productId) {
		this.productId = productId;
	}
	
	@Override
	public String toString() {
		return "Product [id=" + id + ", clientId=" + clientId + ", productId=" + productId + ", quantity=" + quantity + "]";
	}
}
