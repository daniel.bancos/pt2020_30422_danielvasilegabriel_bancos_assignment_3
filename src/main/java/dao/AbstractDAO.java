package dao;

import java.util.logging.Logger;

import connection.ConnectionFactory;

import java.beans.IntrospectionException; 
import java.beans.PropertyDescriptor; 
import java.lang.reflect.Field; 
import java.lang.reflect.InvocationTargetException; 
import java.lang.reflect.Method; 
import java.lang.reflect.ParameterizedType; 
import java.sql.Connection; 
import java.sql.PreparedStatement; 
import java.sql.ResultSet; 
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList; 
import java.util.List; 
import java.util.logging.Level; 
import java.util.logging.Logger;

public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
	
	private final Class<T> type;
	
	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	/**
	 * Creates a query based on the type of T
	 * @param field
	 * @return a string query
	 */
	
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		//sb.append(" management.");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}
	
	/**
	 * Creates a query for inserting elements based on the type of T
	 * @return a string query
	 */
	
	private String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		//private static final String insertStatementString = "INSERT INTO client (name,address)" + " VALUES (?,?)";
		sb.append("INSERT ");
		sb.append("INTO ");
		sb.append(" management.");
		sb.append(type.getSimpleName() + " ");
		String fields = "";
		String values = "";
		fields += "(";
		values += "(";
		for(Field field : type.getDeclaredFields()) {
			if(field.getName().equals("id")) {
				continue;
			}
			fields += field.getName() + ',';
			values += "?,";
		}
		fields = fields.substring(0, fields.length() - 1);
		fields += ')';
		values = values.substring(0, values.length() - 1);
		values += ')';
		sb.append(fields);
		sb.append(" VALUES ");
		sb.append(values);
		
		return sb.toString();
	}
	
	/**
	 * Searches for an element in a table specified by the type of T
	 * @param id
	 * @return object of type T
	 */
	
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1,  id);;
			resultSet = statement.executeQuery();
			return createObjects(resultSet).get(0);
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	/**
	 * Method to insert an object of type T into a table specified by the type of T
	 * @param obj
	 * @return the id of the inserted element
	 */
	
	public int insert(T obj) {
		//System.out.println(createInsertQuery());
		
		Connection connection = null;
		PreparedStatement insertStatement = null;
		String query = createInsertQuery();
		System.out.println(query);
		int insertedId = -1;
		
		try {
			connection = ConnectionFactory.getConnection();
			insertStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			for(Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				if(field.getName().equals("id")) {
					continue;
				}
				T value = null;
				
				try {
					value = (T) field.get(obj);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				insertStatement.setString(k, value.toString());
				k++;
				field.setAccessible(false);
			}
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);
		}
		
		return insertedId;
	}
	
	/**
	 * Deletes an element of type T form a table specified by the type of T
	 * @param id
	 */
	
	public void delete(int id) {
		Connection connection = null;
		PreparedStatement deleteStatement = null;
		String query = "DELETE FROM " + type.getSimpleName() + " WHERE id = ?";
		try {
			connection = ConnectionFactory.getConnection();
			deleteStatement = connection.prepareStatement(query);
			deleteStatement.setInt(1,  id);
			
			deleteStatement.executeUpdate();
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * Creates a list of objects of type T based on a ResultSet object
	 * @param resultSet
	 * @return a list of objects
	 */
	
	public List<T> createObjects(ResultSet resultSet){
		List<T> list = new ArrayList<T>();
		
		
			try {
				while(resultSet.next()) {
					T instance = type.getDeclaredConstructor().newInstance();
					for(Field field : type.getDeclaredFields()) {
						Object value = resultSet.getObject(field.getName());
						PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
						Method method = propertyDescriptor.getWriteMethod();
						method.invoke(instance, value);
					}
					list.add(instance);
				}
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return list;
		
	}
}
