package dao;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import connection.ConnectionFactory;
import model.Order;

public class OrderDAO extends AbstractDAO<Order> {
	
	/**
	 * Creates a PDF file based on the tables order, client and product
	 * @param fileNb representing the number of the file generated
	 */
	
	public void reportOrder(int fileNb) {
		
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Order_Report_" + Integer.toString(fileNb) + ".pdf"));
		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		document.open();
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk = new Chunk("Order report\n", font);
		try {
			document.add(new Paragraph(chunk));
			document.add(new Paragraph("\n"));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT order.id, client.name, product.name, order.quantity FROM management.order JOIN client ON (client.id = order.clientId) JOIN product ON (product.id = order.productId)";
		PdfPTable table = new PdfPTable(4);
		addTableHeader(table);
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				addRows(table, Integer.toString(resultSet.getInt("id")), resultSet.getString("client.name"), resultSet.getString("product.name"), Integer.toString(resultSet.getInt("quantity")));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:ReportOrder " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	/**
	 * Creates the header of the table table
	 * @param table
	 */
	
	private void addTableHeader(PdfPTable table) {
    Stream.of("id", "client", "product", "quantity")
      .forEach(columnTitle -> {
        PdfPCell header = new PdfPCell();
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        header.setBorderWidth(2);
        header.setPhrase(new Phrase(columnTitle));
        table.addCell(header);
    });
	}
	
	/**
	 * Adds a row to the table table
	 * @param table
	 * @param id
	 * @param client
	 * @param product
	 * @param quantity
	 */
	
	private void addRows(PdfPTable table, String id, String client, String product, String quantity) {
    table.addCell(id);
    table.addCell(client);
    table.addCell(product);
    table.addCell(quantity);
	}
}
