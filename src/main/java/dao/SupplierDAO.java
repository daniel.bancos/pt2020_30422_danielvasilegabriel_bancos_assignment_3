package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import connection.ConnectionFactory;
import model.Product;
import model.Supplier;

public class SupplierDAO extends AbstractDAO<Supplier>{
	
	/**
	 * Searches for the id of the supplier that has the name name
	 * @param name representing the name of the searched supplier
	 * @return an id
	 */
	
	public int getId(String name) {
		int toReturn = -1;
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT * FROM supplier where name = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1,  name);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				toReturn = resultSet.getInt("id");
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:findByNameAndAddress " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		return toReturn;
	}
}
