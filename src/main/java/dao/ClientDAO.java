package dao;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import connection.ConnectionFactory;
import model.Client;
import model.Product;

public class ClientDAO extends AbstractDAO<Client>{
	
	/**
	 * Searches for an item by id in the table client 
	 * @param id representing the id of the searched client
	 * @return an object of type Client
	 */
	
	public Client findByIdRem(int id) {
		Client toReturn = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT * FROM client where id = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1,  id);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				toReturn = new Client(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("address"));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return toReturn;
	}
	
	/**
	 * Searches for an element in the table client and returns an object of type Client if it exists and null otherwise
	 * @param name representing the name of the searched client
	 * @return the id of the object if it exists and -1 otherwise
	 */
	
	public int checkIfExists(String name) {
		int toReturn = -1;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT * FROM client where name = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1,  name);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				toReturn = resultSet.getInt("id");
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:findByNameAndAddress " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return toReturn;
	}
	
	
	/**
	 * Deletes the client with id clientId
	 * @param clientId representing the id of the client that made an order
	 */
	
	public void deleteFromOrder(int clientId) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = "DELETE FROM management.order WHERE clientId = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1,  clientId);
			statement.executeUpdate();
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:deleteFormOrder " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * Create a PDF file based on the table client
	 * @param fileNb representing the number of the file generated
	 */
	
	public void reportClient(int fileNb) {
		
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Client_Report_" + Integer.toString(fileNb) + ".pdf"));
		} catch (FileNotFoundException | DocumentException e) {
			e.printStackTrace();
		}
		 
		document.open();
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk = new Chunk("Client report\n", font);
		try {
			document.add(new Paragraph(chunk));
			document.add(new Paragraph("\n"));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT * FROM client";
		PdfPTable table = new PdfPTable(3);
		addTableHeader(table);
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				addRows(table, Integer.toString(resultSet.getInt("id")), resultSet.getString("name"), resultSet.getString("address"));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:ReportClient " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	/**
	 * Creates the header of the table table
	 * @param table
	 */
	
	private void addTableHeader(PdfPTable table) {
    Stream.of("id", "name", "address")
      .forEach(columnTitle -> {
        PdfPCell header = new PdfPCell();
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        header.setBorderWidth(2);
        header.setPhrase(new Phrase(columnTitle));
        table.addCell(header);
    });
	}
	
	
	/**
	 * Adds a row to the table table.
	 * @param table
	 * @param id
	 * @param name
	 * @param address
	 */
	
	private void addRows(PdfPTable table, String id, String name, String address) {
    table.addCell(id);
    table.addCell(name);
    table.addCell(address);
	}
}
