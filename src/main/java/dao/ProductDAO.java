package dao;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import connection.ConnectionFactory;
import model.Product;

public class ProductDAO extends AbstractDAO<Product>{
	
	/**
	 * Checks if an element with name name exists
	 * @param name representing the name of the searched product
	 * @return an object of type Product or null
	 */
	
	public Product checkIfExists(String name) {
		Product toReturn = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT * FROM product where name = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1,  name);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				toReturn = new Product(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("quantity"), resultSet.getInt("price"), resultSet.getInt("supplier_id"));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Product " + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return toReturn;
	}
	
	/**
	 * Searches for a product of id id
	 * @param id representing the id of the searched product
	 * @return an object of type Product
	 */
	
	public Product findByIdRem(int id) {
		Product toReturn = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT * FROM product where id = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1,  id);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				toReturn = new Product(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("quantity"), resultSet.getInt("price"), resultSet.getInt("supplier_id"));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:findByNameAndAddress " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return toReturn;
	}
	
	/**
	 * Updates the quantity of the product product 
	 * @param product representing the object that holds the values of the new fields
	 */
	
	public void update(Product product) {
		int toReturn = -1;
		Connection connection = null;
		PreparedStatement statement = null;
		String query = "UPDATE product SET quantity = ? WHERE id = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1,  product.getQuantity());
			statement.setInt(2,  product.getId());
			statement.executeUpdate();
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Product " + "DAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * Deletes an order which has as product the product of id productId
	 * @param productId representing the id of the product to delete
	 */
	
	public void deleteFromOrder(int productId) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = "DELETE FROM management.order WHERE productId = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1,  productId);
			statement.executeUpdate();
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Product " + "DAO:deleteFormOrder " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * Creates a PDF file based on the tables product and supplier
	 * @param fileNb representing the number of the file generated
	 * @return
	 */
	
	public void reportProduct(int fileNb) {
		
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Product_Report_" + Integer.toString(fileNb) + ".pdf"));
		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		document.open();
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk = new Chunk("Product report\n", font);
		try {
			document.add(new Paragraph(chunk));
			document.add(new Paragraph("\n"));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT product.id, product.name, product.quantity, product.price, supplier.name FROM product"
				+ "	JOIN supplier ON (product.supplier_id = supplier.id)";
		PdfPTable table = new PdfPTable(5);
		addTableHeader(table);
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				addRows(table, Integer.toString(resultSet.getInt("product.id")), resultSet.getString("product.name"), Integer.toString(resultSet.getInt("quantity")), Integer.toString(resultSet.getInt("price")), resultSet.getString("supplier.name"));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Client " + "DAO:ReportProduct " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	/**
	 * Creates the header of the table table
	 * @param table
	 */
	
	private void addTableHeader(PdfPTable table) {
    Stream.of("id", "name", "quantity", "price", "supplier")
      .forEach(columnTitle -> {
        PdfPCell header = new PdfPCell();
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        header.setBorderWidth(2);
        header.setPhrase(new Phrase(columnTitle));
        table.addCell(header);
    });
	}
	
	/**
	 * Adds a row to the table table
	 * @param table
	 * @param id
	 * @param name
	 * @param quantity
	 * @param price
	 * @param supplier
	 */
	
	private void addRows(PdfPTable table, String id, String name, String quantity, String price, String supplier) {
    table.addCell(id);
    table.addCell(name);
    table.addCell(quantity);
    table.addCell(price);
    table.addCell(supplier);
	}
}	
